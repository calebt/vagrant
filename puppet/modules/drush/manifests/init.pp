# == Class: drush
#
# Install and configures drush.
#
# === Example
#
#  include drush
#
# === Authors
#
# Caleb Thorne <caleb@calebthorne.com>
#
# === Copyright
#
# Copyright 2012 Caleb Thorne <http://www.calebthorne.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

class drush (
  $version = '7.x-5.x'
  ) {

  # Use git to grab the correct version of drush.
  exec {'git-drush':
    cwd     => "/tmp",
    command => "git clone --recursive --branch $version http://git.drupal.org/project/drush.git",
    creates => '/tmp/drush',
    path    => ["/usr/bin", "/usr/sbin"],
    require => Package['git-core'],
  }

  file {'/usr/local/lib/drush':
    ensure  => directory,
    recurse => true,
    purge   => true,
    source  => '/tmp/drush',
    require => Exec['git-drush'],
  }

  file {'/usr/local/bin/drush':
    ensure  => link,
    target  => '/usr/local/lib/drush/drush',
    require => File['/usr/local/lib/drush'],
  }
}
