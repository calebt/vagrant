# == File: default-params.pp
#
# Default site-specific parameters.
#
# Copy this file to params.pp and edit the configuration options for
# your installation.
#
# === Authors
#
# Caleb Thorne <caleb@calebthorne.com>
#
# === Copyright
#
# Copyright 2012 Caleb Thorne <http://www.calebthorne.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

class params {
  # Set the default root password for mysql. Blank for no password.
  $mysql_password = ''

  # A list of one or more mysql databases to create.
  $databases = ['database_name']

  # Set to false to disable drush.
  $drush_enabled = true

  # Set to false to disable xdebug
  $xdebug_enabled = true

  # Set to false to disable git.
  $git_enabled = true

  # If git is enabled, the default git user name.
  $git_user = ''

  # If git is enabled, the default git user email.
  $git_email = ''

  # If git is enabled, show colorized output.
  $git_colorize = true

  # Enable postfix for local email.
  $postfix_enabled = true
}
