# == File: init.pp
#
# Configure the vagrant server.
#
# === Authors
#
# Caleb Thorne <caleb@calebthorne.com>
#
# === Copyright
#
# Copyright 2012 Caleb Thorne <http://www.calebthorne.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

include params
include apt

# Install apache and common modules.
class {"apache": }
apache::module {"rewrite": }

# Configure some apache default vhost settings.
augeas {'set-apache-vhost-default':
  context => '/files/etc/apache2/sites-available/default',
  changes => [
    'set VirtualHost/*[self::directive="ServerAdmin"]/arg vagrant@localhost',
    'set VirtualHost/directive LogLevel',
    'set VirtualHost/*[self::directive="LogLevel"]/arg notice',
    'set VirtualHost/Directory[arg =~ regexp(\'"?/var/www/?"?\')]/*[self::directive=\'AllowOverride\']/arg All',
  ],
  require => Package['apache'],
  notify  => Service['apache'],
}

# Install php and some common modules.
class {"php": }
php::module {"curl": }
php::module {"gd": }
php::module {"mysql": }
php::module {"xsl": }
php::module {"apc":
  module_prefix => "php-"
}

# Set some php configurations.
augeas {'set-php-ini-values-apache':
  context => '/files/etc/php5/apache2/php.ini',
  changes => [
    'set PHP/max_execution_time 300',
    'set PHP/max_input_time 300',
    'set PHP/memory_limit 1024M',
    'set PHP/error_reporting "E_ALL | E_STRICT"',
    'set PHP/display_errors On',
    'set PHP/display_startup_errors On',
    'set PHP/html_errors On',
    'set PHP/post_max_size 256M',
    'set PHP/file_uploads On',
    'set PHP/upload_max_filesize 256M',
    'set Date/date.timezone America/Denver',
  ],
  require => Package['php'],
  notify  => Service['apache'],
}

augeas {'set-php-ini-values-cli':
  context => '/files/etc/php5/cli/php.ini',
  changes => [
    'set PHP/max_execution_time "300"',
    'set PHP/max_input_time "300"',
    'set PHP/memory_limit "1024M"',
    'set PHP/error_reporting "E_ALL | E_STRICT"',
    'set PHP/display_errors "On"',
    'set PHP/display_startup_errors "On"',
    'set Date/date.timezone America/Denver',
  ],
  require => Package['php'],
  notify  => Service['apache'],
}

# Install and configure xdebug.
if $params::xdebug_enabled == true {
  php::pecl::module {"xdebug": }
  augeas {'set-xdebug-ini-values':
    context => '/files/etc/php5/conf.d/xdebug.ini',
    changes => [
      'set Xdebug/xdebug.profiler_enable_trigger 1',
    ],
    require => Package['php'],
    notify  => Service['apache'],
  }
}

# Install and configure MySQL.
class {"mysql":
  root_password => $params::mysql_password,
}

# Create the databases.
define databases {
  exec {"create-database-${name}":
    unless  => "mysql ${name}",
    command => "mysql -e \"CREATE DATABASE ${name};\"",
    require => Service["mysql"],
    path    => ['/usr/bin', '/usr/sbin'],
  }
}
databases{$params::databases:}

# Set some mysql php configurations.
augeas {'set-php-ini-values-mysql':
  context => '/files/etc/php5/apache2/php.ini',
  changes => [
    'set MySql/mysql.default_host localhost',
    'set MySql/mysql.default_user root',
    'set MySql/mysql.default_password vagrant',
  ],
  require => [Package['php'], Package['mysql-server']],
  notify  => Service['apache'],
}

# Install and configure drush.
if $params::drush_enabled == true {
  php::pear::module {"Console_Table":
    use_package => 'no',
  }
  class {"drush": }
}

# Install and configure git.
if $params::git_enabled == true {
  class {"git": }

  # Git user info.
  if $params::git_user != '' {
    git::config {'user.name':
      ensure  => present,
      content => "${params::git_user}",
      user    => 'vagrant',
    }
  }

  if $params::git_email != '' {
    git::config {'user.email':
      ensure  => present,
      content => "${params::git_email}",
      user    => 'vagrant',
    }
  }

  # Colorized output.
  git::config {'color.ui':
    scope   => 'system',
    content => $params::git_colorize,
  }
}

# Install and configure postfix for local email.
if $params::postfix_enabled == true {
  class {"postfix": }
  package {"postfix-pcre":
    require => Package['postfix'],
  }

  # Set some postfix configurations for local development.
  augeas {'set-main-cf-values-postfix':
    context => '/files/etc/postfix/main.cf',
    changes => [
      'set luser_relay vagrant@localhost',
      'clear local_recipient_maps',
      'set mydestination pcre:/etc/postfix/mydestination',
    ],
    require => [Package['postfix'], Package['postfix-pcre']],
    notify  => Service['postfix'],
  }

  # Add the mydestination file.
  file {"/etc/postfix/mydestination":
    ensure => 'present',
    require => Package['postfix-pcre'],
    content => "/.*/\tACCEPT",
  }

  # Add a mail alias for root: vagrant
  mailalias {"vagrant":
    ensure    => 'present',
    name      => 'root',
    recipient => 'vagrant',
    require   => Package['postfix'],
  }

  # Install mutt for viewing local mail.
  package {"mutt":
    require => Package['postfix'],
  }
}
